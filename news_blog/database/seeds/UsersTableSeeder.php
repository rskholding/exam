<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Админ',
            'login' => 'admin',
            'email' => 'admin@mail.ru',
            'password' => bcrypt('123456'),
        ]);
    }
}
