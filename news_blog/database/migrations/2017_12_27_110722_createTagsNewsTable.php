<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsNewsTable extends Migration
{
    private $table = 'tags_blog';

    private $defaultTags = ['Спорт', 'Новость', 'Здоровье'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('title_tag')->nullable();
                $table->timestamps();
            });
        }
        foreach ($this->defaultTags as $value) {
            \DB::table($this->table)->insert([
                'title_tag' => $value,

            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable($this->table)) {
            Schema::drop($this->table);
        }
    }
}
