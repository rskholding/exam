<?php


/** Форма аутентификации */
Route:: get('/',  function() {
    if (Session::has('user_id')) {
        return redirect()->intended('news_blog/lists');
    }
    return view('auth.login');
});
/** Аутентификация пользователя */
Route::match(['get', 'post'],'login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);

Route::group(
    [
        'prefix' => 'news_blog',
        'middleware' => 'auth',
    ], function () {
        Route::get('logout',['as'=>'logout', 'uses'=>'Auth\LoginController@logout']);
        Route::get('/lists', [
            'as'   => 'news.list',
            'uses' => 'News\NewsController@index',
        ]);
        Route::get('/add', [
            'as' => 'news.add',
            'uses' => 'News\NewsController@add',
        ]);
        Route::post(
            '/{id}/store',
            [
                'as' => 'news.store',
                'uses' => 'News\NewsController@store'
            ]
        );
        Route::get(
            '/{id}/edit',
            [
                'as' => 'news.edit',
                'uses' => 'News\NewsController@edit'
            ]
        );
        Route::get(
            '/{id}/delete',
            [
                'as' => 'news.delete',
                'uses' => 'News\NewsController@delete'
            ]
        );
});