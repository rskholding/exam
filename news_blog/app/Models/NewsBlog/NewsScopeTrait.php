<?php

namespace App\Models\NewsBlog;

/**
 * Trait NewsScopeTrait
 * @package App\Models\NewsBlog
 */
trait NewsScopeTrait
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $value
     * @return mixed
     */
    public function scopeTitle($query, $value)
    {
        if (!is_null($value)) {
            return $query->where('title_news', 'ILIKE', "%{$value}%");
        }
    }
}
