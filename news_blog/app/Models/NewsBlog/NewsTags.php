<?php

namespace App\Models\NewsBlog;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsTags
 * @package App\Models\NewsBlog
 */
class NewsTags extends Model
{
    protected $table = 'tags_blog';

    protected $fillable = [
        'title_tag',
    ];

    public function news()
    {
        return $this->belongsToMany('\App\Models\NewsBlog\News', 'tags_rel_news');
    }
}
