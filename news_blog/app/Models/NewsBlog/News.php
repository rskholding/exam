<?php

namespace App\Models\NewsBlog;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @package App\Models\NewsBlog
 */
class News extends Model
{
    use NewsScopeTrait;

    protected $table = 'news_blog';

    protected $fillable = [
        'title_news',
        'text_news',
    ];

    public function tags()
    {
        return $this->belongsToMany('\App\Models\NewsBlog\NewsTags', 'tags_rel_news', 'news_id', 'tags_id');
    }

}
