<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Метод для аутентификации пользователя в системе
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function login(Request $request){
        $input = e(Input::get('email_or_username'));
        $user = User::where('email', $input)->orWhere('login', $input)->first();
        if ($request->isMethod('POST') ) {
            if($user) {
                $session = Session::has('user_id')
                    ? Session::get('user_id')
                    : $request->session()->put('user_id', Auth::id() . uniqid());
                if (Auth::attempt(['email' => $user->email, 'password' => $request->get('password')], $session)) {
                    return redirect()->intended('news_blog/lists');
                }
            }
        }
        return view('auth.login')
            ->withErrors(array('email_or_username'=> 'Логин или пароль заполнены неверно','password' => 'Логин или пароль заполнены неверно',));

    }

    /**
     * @return выход из проекта
     */
    public function logout(Request $request)
    {
        if (Auth::check()) {
            $request->session()->forget('user_id');
            Auth::logout();
        }

        return redirect('/');
    }
}
