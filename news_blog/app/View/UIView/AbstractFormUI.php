<?php

namespace App\View\UIView;

use App\View\Components\Filter\DefaultFilter;
use App\View\Components\Form\DefaultForm;
use App\View\Components\Table\DefaultTable;
use Illuminate\Http\Request;

abstract class AbstractFormUI
{
    /** @var DefaultFilter */
    protected $filter;

    /** @var DefaultTable */
    protected $table;

    /** @var DefaultForm */
    protected $form;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
