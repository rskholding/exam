<?php

namespace App\View\UIView;

use App\View\Components\Constructor;
use App\View\Components\Filter\DefaultFilter;
use App\View\Components\Form\DefaultForm;
use App\View\Components\Table\DefaultTable;

/**
 * Class NewsUI
 * @package App\View\UIView
 */
class NewsUI extends AbstractFormUI
{
    public function initList()
    {
        $this->filter = Constructor::createElement((new DefaultFilter())->getElementName());
        $this->filter->setTitle('Фильтр: Новости');
        $this->filter->addField('title_news', [
            'title' => 'Заголовок новости',
            'type' => 'text',
        ]);
        $this->table = Constructor::createElement((new DefaultTable())->getElementName());
        $this->table->setTitle('Отчет: Новости');
        $this->table->addButton([
            'name' => 'Добавить новость',
            'type' => DefaultTable::BUTTON_ADD,
            'href' => route('news.add'),
        ]);
        $this->table->addColumn('index', [
            'column' => 'index',
            'name' => '№ п.п.',
        ]);
        $this->table->addColumn('title_news', [
            'column' => 'title_news',
            'name' => 'Заголовок',
        ]);
        $this->table->addColumn('text_news', [
            'column' => 'text_news',
            'name' => 'Содержание'
        ]);
        $this->table->addColumn('tags', [
            'column' => 'tags',
            'name' => 'Теги',
        ]);
        $this->table->addColumn('created_at', [
            'column' => 'created_at',
            'name' => 'Дата публикации',
        ]);
        return $this;
    }

    public function getViewList()
    {
        return view('pages.news.list', [
            'table' => $this->table->getViewTable(),
            'filter' => $this->filter->getViewFilter(),
        ]);
    }

    public function initForm($values = [], $tags = [])
    {
        $this->form = Constructor::createElement((new DefaultForm())->getElementName());
        $this->form->setTitle('Просмотр/редактирование новости');
        $this->form->addField('title_news', [
            'title' => 'Заголовок новости',
            'type' => 'text',
            'value' => $values['title_news'],
        ]);
        $this->form->addField('text_news', [
            'title' => 'Содержание новости',
            'type' => 'textarea',
            'value' => $values['text_news'],
        ]);
        $chooseTags = [];
        foreach ($values['tags'] as $value) {
            $chooseTags[] = $value['id'];
        }
        $allTags = [];
        foreach ($tags as $tag) {
            $allTags[$tag->id] = $tag->title_tag;
        }

        $this->form->addField('tags', [
            'title' => 'Теги',
            'type' => 'multiple',
            'value' => $chooseTags,
            'items' => $allTags,
        ]);
        $this->form->setButtons(
            [
                [
                    'type' =>  DefaultForm::BUTTON_APPLY,
                    'action' => route('news.store', ['id' => $values['id']]),
                ],
                [
                    'type' => DefaultForm::BUTTON_BACK,
                    'action' => route('news.list'),
                ]
            ]);
        $this->form->setAction('news_blog/'. $values['id'] . '/store');
    }

    public function getViewNews()
    {
        return view('pages.news.form', [
            'form' => $this->form->getViewForm(),
        ]);
    }

    /**
     * * @param \Illuminate\Database\Eloquent\Builder $query
     */
    public function filter($query = '')
    {
        if ($this->filter->isFilter()) {
            $query->title($this->filter->filterField('title_news'));
        };
        return $query;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function ajax()
    {
        return view('ajax.news', [
            'table' => $this->table->getViewTable(),

        ]);
    }
}
