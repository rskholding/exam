<?php

namespace App\View\Components\Table;

use App\View\Components\AbstractConstructor;
use App\View\Components\Elements\Button;
use App\View\Components\Elements\Color;
use App\View\Components\Elements\Icon;
use Illuminate\Support\Facades\Request;

class DefaultTable extends AbstractConstructor
{
    const BUTTON_ADD = 'add';
    const BUTTON_EDIT = 'edit';
    const BUTTON_DELETE = 'delete';

    protected $name = 'table';

    private $buttons = [];

    private $columns = [];

    private $rows = [];

    protected $pagination;

    private $perPage;

    public function addButton($button = [])
    {
        switch ($button['type']) {
            case self::BUTTON_ADD:
                $buttons = new Button($button['name'], $button['href'], self::BUTTON_ADD);
                $buttons->setIcon(!empty($button['icon'])? $button['icon'] : Icon::ADD)->setPlace('right');
                $this->buttons[] = $buttons;
                break;
        }
        return $this;
    }

    public function addColumn($idColumn, $columnValue = [])
    {
        $this->columns[$idColumn] = $columnValue;
        return $this;
    }

    public function addRow($data, $buttons = [])
    {
        $row = new TableRow();
        if(!empty($buttons)) {
            foreach ($buttons as $button) {
                $action = [
                    'type' => $button['type'],
                    'href' => $button['href'],
                    'name' => !empty($button['name']) ? $button['name'] : '',
                    'icon' => !empty($button['icon']) ? $button['icon'] : '',
                    'color' => !empty($button['button']) ? $button['button'] : '',
                ];
                switch ($button['type']) {
                    case DefaultTable::BUTTON_EDIT:
                        $action['name'] = $action['name'] ? $action['name'] : 'Редактировать запись';
                        $action['icon'] = $action['icon'] ? $action['icon'] : Icon::CREATE;
                        $action['color'] = $action['color'] ? $action['color'] : Color::GREEN;
                        break;
                    case DefaultTable::BUTTON_DELETE:
                        $action['name'] = $action['name'] ? $action['name'] : 'Удалить запись';
                        $action['icon'] = $action['icon'] ? $action['icon'] : Icon::DELETE;
                        $action['color'] = $action['color'] ? $action['color'] : Color::RED;
                        break;
                };
                $row->addAction($action);
            }


            $this->isActionColumn = true;
        }
        foreach ($data as $key => $value) {
            $cell = new TableCell($value);
            $row->addCell($key, $cell);

        }
        $this->rows[] = $row;
        return $this->rows;
    }

    public function setPagination($pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }

    public function setPerPage($default = 5)
    {
        return $this->perPage = ($this->perPage ?: (Request::input('perPage') ?: $default));
    }



    public function getViewTable()
    {
        return [
            'title' => $this->title,
            'icon'  => $this->icon,
            'buttons' => $this->buttons,
            'columns' => $this->columns,
            'rows' => $this->rows,
            'pagination' => $this->pagination,
        ];
    }
}