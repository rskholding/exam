<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 26.12.17
 * Time: 22:54
 */

namespace App\View\Components\Table;


class TableRow
{
    private $action = [];
    /** @var TableCell[] */
    private $columns = [];

    public function addAction($actions = [])
    {
        $this->action[] = $actions;
    }

    public function getActions()
    {
        return $this->action;
    }

    public function addCell($column, $cell)
    {
        if (!is_object($cell)) {
            $cell = new TableCell($cell);
        }
        $this->columns[$column] = $cell;
        return $this;
    }

    public function getCell($code)
    {
        return !empty($this->columns[$code]) ? $this->columns[$code] : null;
    }

}