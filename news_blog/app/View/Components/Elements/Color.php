<?php

namespace App\View\Components\Elements;

class Color
{
    const GREEN_LIGHTER = '#26a69a';
    const GREEN = 'green';
    const BLUE = 'blue';
    const RED = 'red';
    const GREY = 'grey';

    protected $color = '';

    public function __construct($color)
    {
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }
}
