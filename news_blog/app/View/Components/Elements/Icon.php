<?php

namespace App\View\Components\Elements;

class Icon
{
    const SAVE = 'save';
    const BACK = 'reply';
    const ADD = 'add';
    const CREATE = 'create';
    const DELETE = 'delete';

    protected $icon = '';

    public function __construct($icon)
    {
        $this->icon = $icon;
    }

    public function getIcon()
    {
        return $this->icon;
    }
}