<?php

namespace App\View\Components;

abstract class AbstractConstructor implements IConstructor
{
    protected $name = '';

    protected $title = 'Таблица';

    protected $icon = 'dehaze';

    public function setTitle($title = '')
    {
        return $this->title = $title;
    }

    public function setIconTitle($iconTitle = '')
    {
        return $this->icon = $iconTitle;
    }

    public function getElementName()
    {
        return $this->name;
    }
}