<?php

namespace App\View\Components;


interface IConstructor
{
    public function setTitle($title = '');

    public function setIconTitle($iconTitle = '');
}
