<?php

namespace App\Service;

use App\Models\NewsBlog\News;
use App\Models\NewsBlog\NewsTags;

/**
 * Class NewsBlogService
 * @package App\Service
 */
class NewsBlogService
{
    private $relation = [
        'tags',
    ];

    /**
     * @param News $newsBlog
     * @param array $data
     * @return bool
     */
    public function saveNewsBlog(News $newsBlog, $data = [])
    {
        $newsBlog->fill($data);
        if ($newsBlog->push()) {
            $newsBlog->tags()->detach();
            foreach (array_get($data, 'tags', []) as $id) {
                $tag = NewsTags::find($id);
                $newsBlog->tags()->attach($tag);
            }
        }
        return true;
    }

    /**
     * @param int $id
     * return News
     */
    public function getNewsBlogById($id)
    {
        return News::query()->find($id);
    }

    public function getNewsBlogQuery()
    {
        return News::query()->with($this->relation)->orderBy('updated_at', 'desc');
    }

    /**
     * @param News $news
     * @return bool
     */
    public function deleteNews(News $news)
    {
        $news->tags()->detach();
        $news->delete();
        return true;
    }

    /**
     * Весь список тегов по новостям
     * @return NewsTags
     */
    public function getAllTags()
    {
        return NewsTags::all();
    }
}
