@if (\Session::has('message') or \Session::has('error'))
    <div class="row">
        <div class="col m12">
            <div class="card">
                <div class="card-panel {{ \Session::has('message') ? 'green' : 'red' }}">
                    @if (\Session::has('message'))
                        <div>
                            {{ \Session::get('message') }}
                        </div>
                    @endif
                    @if (\Session::has('error'))
                        <div>
                            {{ \Session::get('error') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif