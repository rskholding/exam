
@extends('layout.source')
@section('body')
<div class="navbar-fixed">
    <nav class="main-navbar">
        <div class="nav-wrapper">
            <ul class="right">
                <li><a href="{{route('logout')}}"><i class="material-icons left">directions_run</i>Выйти</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="main">
    @include('message.message')
    @yield('content')
</div>
<footer class="page-footer custom-footer">
    <div class="footer-copyright">
        <div class="container center-align">
            <b>© 2017 Copyright News Blog</b>
        </div>
    </div>
</footer>
@endsection