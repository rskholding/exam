<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>News blog</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/main.min.css">
    <script src="/assets/js/jquery/dist/jquery.min.js"></script>
</head>
<body>
    @yield('body')
<script src="/assets/js/plugin/materialize.min.js"></script>
</body>
</html>