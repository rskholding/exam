@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col m12">
            @include('common.filter.filter')
        </div>
    </div>
    <div class="row">
        <div class="col m12" id="table">
            @include('common.table.table')
        </div>
    </div>
@endsection