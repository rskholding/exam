<div class="row" style="margin-bottom: 0">
    <div class="col m12">
        <div class="card">
            <div class="card-panel blue-grey lighten-4 head-title">
                <span ><i class="material-icons left">{{ $filter['icon'] }}</i>{{ $filter['title'] }}</span>
            </div>
                <form action="{{$filter['action']}}" method="get">
                    <div class="row">
                        <div class="input-field col s12">
                        @include('common.elements.fields', [
                        'field' => $filter['fields']['title_news']
                    ])
                        </div>
                    </div>
            <div class="card-action">

                <button class="waves-effect waves-light blue btn"><i class="material-icons left">search</i>Найти</button>
            </div>
                </form>
        </div>
    </div>
</div>
<script>
var filter = $("#{{$filter['fields']['title_news']['id']}}");
filter.keyup(function (e) {
    var dataValue = $('form').serialize();
    $.ajax({
        url: '{{Request::url()}}?' + dataValue,
        type: 'GET',
        data: dataValue,
        dataType: 'json',
        cache: false,
        success: function(res, st, response) {
            $('#table').replaceWith(response.responseJSON);
        },
        error: function (response) {

        }
    });


});
</script>