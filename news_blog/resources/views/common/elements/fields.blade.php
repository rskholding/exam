@if ($field['type'] === 'text')
        <input id ="{{$field['id']}}" type="text" class="validate" value="{{ isset($field['value']) ? $field['value'] : '' }}" name="{{$field['name']}}">
        <label>{{$field['title']}}</label>
@elseif ($field['type'] === 'textarea')
        <textarea id ="{{$field['id']}}" class="validate materialize-textarea"  name="{{$field['name']}}">{{ isset($field['value']) ? $field['value'] : '' }}</textarea>
        <label>{{$field['title']}}</label>

@elseif ($field['type'] === 'multiple')
        <select multiple name="{{ $field['name'] }}[]">
            <option value="" disabled selected>Выберите записи</option>
            @foreach ($field['items'] as $id => $value)
                <option value="{{ $id }}"{{ in_array($id, $field['value']) ?' selected':'' }}>{{ $value }}</option>
            @endforeach
        </select>
        <label>{{$field['title']}}</label>
        <script>
            $(document).ready(function() {
                $('select').material_select();
            });
        </script>

@endif
