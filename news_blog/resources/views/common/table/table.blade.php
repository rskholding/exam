<div class="row">
    <div class="col m12">
        <div class="card">
            <div class="card-panel blue-grey lighten-4 head-title">
                <span ><i class="material-icons left">{{$table['icon']}}</i>{{$table['title']}}</span>
            </div>
            <div class="card-content content">
                @foreach($table['buttons'] as $button)
                    {!! $button->getViewButton() !!}
                @endforeach

                <table class="centered striped">
                    <thead>
                        <tr>
                            <th style="width: 140px; min-width: 140px">Действие</th>
                            @foreach($table['columns'] as $column)
                                <th>{{$column['name']}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                    <?php /** @var \App\View\Components\Table\TableRow $row */ ?>
                    @foreach($table['rows'] as $row)
                        <tr>
                            <td style="width: 140px; min-width: 140px">
                                @foreach ($row->getActions() as $action)
                                    <a class="waves-effect waves-light btn-floating {{$action['color']}}" title="{{$action['name']}}" href="{{$action['href']}}"><i class="material-icons tiny">{{$action['icon']}}</i></a>
                                @endforeach
                            </td>
                    @foreach ($table['columns'] as $column)
                        <td>{{$row->getCell($column['column'])->getValue()}}</td>

                    @endforeach
                    </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

        </div>
        @if ($table['pagination'])
            {!! $table['pagination'] !!}
        @endif
    </div>

</div>