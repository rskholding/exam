@extends('main_login')

@section('content')
    <div class="row">
        <div class="col s12">
            <h4 class="center-align">Авторизация</h4>
        </div>
    </div>
    <div class="row">
    <form action="{{ url('/login') }}" class="col s12" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset>
            <div class="{{ $errors->has('email') ? ' red-text' : '' }}">
                <div class="input-field col s12">
                    <i class="material-icons prefix" style="color: gray">person</i>
                    <input type="text" name="email_or_username" placeholder="логин"/>
                </div>
            </div>
            <div class="{{ $errors->has('password') ? ' red-text' : '' }}">
                <div class="input-field col s12">
                    <i class="material-icons prefix " style="color: gray">lock</i>
                    <input type="password" name="password" placeholder="пароль"/>
                </div>
                @if ($errors->has('password'))
                    <span class="center-align red-text col s12" style="margin:19px 0">
                        <b>{{ $errors->first('password') }}</b>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn waves-effect waves-light col s12"><i class="material-icons right ">forward</i> Начать работу</button>
        </fieldset>
    </form>
    </div>
@endsection
