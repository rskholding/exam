var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minCss = require('gulp-cssmin');

var resource = [
    'resources/assets/sass/main.sass',
    "resources/assets/sass/materialize-src/sass/materialize.scss"
];
gulp.task('run-sass', function () {
    return gulp.src(resource)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix : '.min'}))
        .pipe(gulp.dest('public/assets/css'))
});


gulp.task('watcher', function () {
    gulp.watch(resource,['sass']);
});