<?php

/**
 * Подключение к БД
 */
global $conn;
function connectToDB()
{
    $dbconn = "pgsql:host=localhost;dbname=postgres;user=task;password=123456";

    try {
        $conn = new PDO($dbconn);
        if ($conn) {
            echo "Connected to the <strong>{$dbconn}</strong> database successfully!\n";
            $sql = "CREATE TABLE student (id int, name VARCHAR, sex CHAR)";
            $conn->exec($sql);
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    return $conn;
}

/**
 * @param PDO $conn
 * @param $result
 */
function getResult($conn, $result)
{
    $result = $conn->query($result);
    $result->rowCount();
    if ($result->rowCount()) {
        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            print_r($row);
        }
    } else {
        echo "\nСовпадение не найдено";
    }
}

/**
 * @param PDO $conn
 * @param $result
 */
function getInsert($conn, $result)
{
    $conn->exec($result);
}