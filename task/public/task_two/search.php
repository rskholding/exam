<?php
// Задача номер 2
function scanDirectories($rootDir, $searchFile = 'php', $allData = [])
{
    $invisibleFileNames = [".", ".."];
    $dirContent = scandir($rootDir);
    foreach ($dirContent as $key => $content) {
        $path = $rootDir . '/' . $content;
        if (!in_array($content, $invisibleFileNames)) {
            if (is_file($path) && is_readable($path)) {

                $fileWithoutPath = preg_replace("/.*\//", "", $path);
                if ((strpos($fileWithoutPath, (string)$searchFile) !== false) && preg_match("/^[a-zA-Z0-9]+\.(?:[^A-Z 0-9]){2,4}$/", $fileWithoutPath)) {
                    $allData[] = $path;
                }
            }
            elseif (is_dir($path) && is_readable($path)) {
                $allData = scanDirectories($path, $searchFile, $allData);
            }
        }
    }
    return $allData;
}

$directory = $argv[1];
$file = $argv[2];
$files = scanDirectories($directory, $file, []);
print_r(!empty($files)? $files : 'Файл не найден') ;

?>